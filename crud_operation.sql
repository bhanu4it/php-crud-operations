-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 21, 2017 at 07:20 PM
-- Server version: 5.6.25
-- PHP Version: 5.5.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crud_operation`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `crdate` date NOT NULL,
  `ip_address` varchar(18) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `empid` varchar(100) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `food_preference` varchar(255) NOT NULL,
  `vehicle` varchar(50) NOT NULL,
  `address` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `crdate`, `ip_address`, `user_agent`, `name`, `empid`, `gender`, `food_preference`, `vehicle`, `address`, `status`) VALUES
(2, '0000-00-00', '', '', 'Venky', 'abc123', 'Male', '', 'on', '  sample address', '0'),
(3, '0000-00-00', '', '', 'bhanu', 'abc123', 'Male', '', 'on', '  test', '0'),
(4, '0000-00-00', '', '', 'sdfsdf', 'abc123', 'Male', 'Veg', 'on', '  ddd', '0'),
(6, '0000-00-00', '', '', 'Deepika', 'emp123', 'Female', 'Veg', 'Array', ' hyderabad', '1'),
(7, '0000-00-00', '', '', 'Gopi mohan', 'emp123', 'Male', 'Veg', 'Array', ' hyderabad', '1'),
(8, '0000-00-00', '', '', 'bhanu', 'abc123', 'Male', 'Veg', 'Scooty,', 'hyderabad', '1'),
(9, '0000-00-00', '', '', 'bhanu', 'abc123', 'Male', 'Veg', '', 'hyderabad', '1'),
(10, '0000-00-00', '', '', 'bhanu', 'abc123', 'Male', 'Veg', '', 'hyderabad', '1'),
(11, '0000-00-00', '', '', 'bhanu', 'abc123', 'Male', 'Veg', '', 'hyderabad', '1'),
(13, '0000-00-00', '', '', 'Murthy', 'ebc123', 'Male', 'Veg', 'Bike,Cycle,Scooty', 'Delhi', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
